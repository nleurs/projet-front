import Vue from 'vue'
import Router from 'vue-router'
// import HelloWorld from '@/components/HelloWorld'
import HomePage from '@/components/HomePage'
import Listing from '@/components/Listing'
import NewList from '@/components/NewList'
import ViewList from '@/components/ViewList'
import NewElem from '@/components/NewElem'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'homePage',
      component: HomePage
    },
    {
      path: '/listing',
      name: 'listing',
      component: Listing
    },
    {
      path: '/new',
      name: 'newList',
      component: NewList
    },
    {
      path: '/list/:name',
      name: 'ViewList',
      component: ViewList
    },
    {
      path: '/list/:name/add',
      name: 'newElem',
      component: NewElem
    }
  ]
})
